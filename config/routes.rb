# == Route Map
#
#         api_v1_todos_path	    GET	     /api/v1/todos(.:format)	                    api/v1/todos#index
#                               POST	   /api/v1/todos(.:format)	                    api/v1/todos#create
#         api_v1_todo_path	    GET	     /api/v1/todos/:id(.:format)	                api/v1/todos#show
#                               PATCH	   /api/v1/todos/:id(.:format)	                api/v1/todos#update
#                               PUT	     /api/v1/todos/:id(.:format)                  api/v1/todos#update
#                               DELETE	 /api/v1/todos/:id(.:format)	                api/v1/todos#destroy
#         api_v1_users_path	    POST	   /api/v1/users(.:format)	                    api/v1/users#create
#         api_v1_login_path	    POST	   /api/v1/login(.:format)	                    api/v1/sessions#create
#         api_v1_logout_path	  DELETE	 /api/v1/logout(.:format)                     api/v1/sessions#destroy
#         api_v1_logged_in_path	GET	     /api/v1/logged_in(.:format)	                api/v1/sessions#logged
#

Rails.application.routes.draw do
  namespace :api do 
    namespace :v1 do 
      resources :todos

      resources :users, only: [:create]

      post '/login', to: 'sessions#create'
      delete '/logout', to: 'sessions#destroy'
      get '/logged_in', to: 'sessions#logged'
    end
  end
end
