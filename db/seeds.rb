# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#

Todo.create(title: "Tarea 1", description: "Descripción tarea 1")
Todo.create(title: "Tarea 2", description: "Descripción tarea 2")