class Api::V1::UsersController < ApplicationController

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      render json: {
        created: true,
        user: @user
      }
    else 
      render render json: {
        status: :unprocessable_entity,
        errors: @user.errors
      } 
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
