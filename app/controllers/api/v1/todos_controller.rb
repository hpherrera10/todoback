class Api::V1::TodosController < ApplicationController

  # GET /api/v1/todos
  def index
    todos = Todo.all

    if(params[:sort_by] != "{}")
      todos = todos.order(params[:sort_by])
    else
      todos = todos.order("created_at ASC")
    end

    render json: todos
  end

  # POST /api/v1/todos
  def create
    todo = Todo.create(todo_param)
    render json: todo
  end

  # PUT /api/v1/todo
  def update
    todo = Todo.find(params[:id])
    todo.update_attributes(todo_param)
    render json: todo
  end

  # DELETE /api/v1/todo
  def destroy
    todo = Todo.find(params[:id])
    todo_title = todo.title
    todo.destroy
    
    render json: {
      status: :ok,
      todo_title: todo_title,
    }

  end
  
  private

  def todo_param
    params.require(:todo).permit(:title, :description)
  end
end
