class Api::V1::SessionsController < ApplicationController

  # POST /api/v1/login
  def create
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      render json: {
        status: :created,
        logged_in: true,
        user: @user
      }
      do_stuff
    else
      render json: { status: 401 }
    end
  end

  # GEST /api/v1/logged_in
  def logged
    if logged_in? && current_user
      render json: {
        logged_in: true,
        user: current_user
      }
    else
      render json: {
        logged_in: false,
        message: 'no such user'
      }
    end
  end

  # DELETE /api/v1/logout
  def destroy
    logout!
    render json: {
      status: 200,
      logged_out: true
    }
  end
end
